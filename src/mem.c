#define _DEFAULT_SOURCE
#define BLOCK_MIN_CAPACITY 24

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size
size_from_capacity( block_capacity
cap );
extern inline block_capacity
capacity_from_size( block_size
sz );

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool

region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}


/* Выделить регион системным вызовом mmap и на его основе создать блок */
static struct region alloc_region(void const *addr, size_t query) {
    block_size size = size_from_capacity((block_capacity) {.bytes = query});
    block_size actual_size = (block_size) {.bytes = region_actual_size(size.bytes)};
    struct region region = {0};
    void *map_reg = map_pages(addr, actual_size.bytes, MAP_FIXED_NOREPLACE);
    region.addr = map_reg;
    region.extends = true;
    if (map_reg == MAP_FAILED) {
        map_reg = map_pages(addr, actual_size.bytes, 0);
        region.addr = map_reg;
        region.extends = false;
        if (map_reg == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    region.size = actual_size.bytes;
    block_init(map_reg, actual_size, NULL);
    return region;
}

static void *block_after(struct block_header const *block);

/* Инициализация кучи, возвращает адрес начала кучи */
void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) {
        return NULL;
    }
    return region.addr;
}



/* --- Разделение блоков (если найденный свободный блок слишком большой )--- */

/* Возвращает true, если блок можно разделить, иначе false */
static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query + offsetof(
    struct block_header, contents ) +BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/* Делит блок, если он большой, возвращает true при успешной операции */
static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        block_size old_block_size = size_from_capacity(block->capacity);
        struct block_header* next_block = block->next;
        block_init(block, (block_size) {.bytes = query + offsetof(struct block_header, contents)}, NULL);
        block_init(block_after(block), (block_size) {.bytes = old_block_size.bytes- size_from_capacity(block->capacity).bytes }, next_block);
        block->next = block_after(block);
        return true;
    }

    return false;

}


/* --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

/* Возвращает true, если второй блок стоит после первого */
static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd
        ) {
    return (void*) snd == block_after(fst);
}

/* Возвращает true, если блоки можно объединить */
static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst != NULL && snd != NULL && fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/* Пытается объединить два блока (блок, который следует после блока в аргументе.
 * Возвращает true при успешной операции */
static bool try_merge_with_next(struct block_header *block) {
    if (mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        return true;
    }
    return false;
}


/* --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    }                   type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    while (block != NULL) {
        while (try_merge_with_next(block));
        if ((block_is_big_enough(sz, block)) && block->is_free) {
            return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = block };
        }
        if (block->next == NULL) {
            return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = block };
        }
        block = block->next;
    }
    return (struct block_search_result) { .type = BSR_CORRUPTED, .block = block };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_search_result  result     = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }

    return result;
}

/* Увеличивает размер кучи и возвращает адрес на новую кучу */
static struct block_header* grow_heap(struct block_header *restrict last, size_t query) {
    struct region reg = alloc_region(block_after(last), query);
    if (reg.addr == NULL) return false;
    last->next = reg.addr;
    if (try_merge_with_next(last)) return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result result = try_memalloc_existing(query,heap_start);
    switch (result.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return result.block;
        case BSR_REACHED_END_NOT_FOUND:
            result = try_memalloc_existing(query, grow_heap(result.block, query));
            if (result.type != BSR_FOUND_GOOD_BLOCK) return NULL;
            return result.block;
        case BSR_CORRUPTED:
        default:
            return result.block;
    }

}

/* Принимает размер, и выделяет память этого размера */
void *_malloc(size_t query) {
    struct block_header *const mem = memalloc(query, (struct block_header *) HEAP_START);
    if (mem) return mem->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}

/* Освобождает выделенную память */
void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
